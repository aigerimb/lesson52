import './App.css';
import React, {Component} from "react";
import Lotto from "./Lotto/Lotto";

class App extends Component {

  createRange = () => {
    const range = [];
    for (let i = 1; i <= 36; i++) {
      range.push(i);
    }
    return range;
  };

  state = {
    condition:
      {
        numbers: [0, 0, 0, 0, 0], range: this.createRange()
      }

  };

  getRandomIndex = (range) => {
    const index = Math.floor((Math.random() * range-1) + 1);
    return index;
  };

  changeNumbers = () => {
    let index;
    const condition = {...this.state.condition};
    let range = [...condition.range];
    const numbers = [...condition.numbers];
    for (let i = 0; i < numbers.length; i++) {
      index = this.getRandomIndex(range.length);
      console.log("index", index);
      numbers[i] = range[index];
      range.splice(index, 1);
      console.log(range);
    }
    console.log(numbers)
    numbers.sort((a, b) => {
      return a-b;
    })
    condition.numbers = numbers;
    this.setState({condition});
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>Play Lotto, Tempt your Fate</h1>
        </header>
        <button className="button" onClick={this.changeNumbers}>New numbers</button>
        <Lotto numbers={this.state.condition.numbers}/>
      </div>
    );
  }

}

export default App;
