import React from "react";
import "./Lotto.css";

const Lotto = props => {
  const arr = props.numbers;
  return (
    <div className="lotto_wrapper">
      {
        arr.map((item, index) => {
          return (
            <span key={index} className="item">{item}</span>
          )
        })
      }
    </div>
  );
};

export default Lotto;